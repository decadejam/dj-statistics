import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import matplotlib.dates as mdates
import csv
import sys
import datetime

def setup_xaxis():
    axis = plt.gca()
    axis.xaxis.set_major_formatter(formatter)
    axis.xaxis.set_major_locator(locator)
    axis.xaxis.set_minor_locator(dlocator)
    plt.xticks(rotation=45, horizontalalignment='right')
    plt.gcf().subplots_adjust(bottom=0.25)

def setup_yaxis():
    axis = plt.gca()
    _, y_top = axis.get_ylim()
    y_top += y_top * 0.1
    axis.set_ylim([0,y_top])

def div(x,y):
    return x / y

dates = []
members_jam = []
submissions = []
members_discord = []

audio = []
artists = []
programmers = []
writers = []
moderators = []

english = []
french = []
spanish = []
german = []

roleplayers = []
community_project = []
without_role = []


with open('decade_jam_stats.csv', 'r') as csvfile:
    plots = csv.reader(filter(lambda row: row[0]!='#', csvfile), delimiter=',')
    for row in plots:
        dates.append(row[0])
        members_jam.append(int(row[1]))
        submissions.append(int(row[2]))
        members_discord.append(int(row[3]))
        audio.append(int(row[4]))
        artists.append(int(row[5]))
        programmers.append(int(row[6]))
        writers.append(int(row[7]))
        moderators.append(int(row[8]))
        english.append(int(row[9]))
        french.append(int(row[10]))
        spanish.append(int(row[11]))
        german.append(int(row[12]))
        roleplayers.append(int(row[13]))
        community_project.append(int(row[14]))
        without_role.append(int(row[15]))


x_dates = [datetime.datetime.strptime(d,"%d/%m/%Y").date() for d in dates]
formatter = mdates.DateFormatter("%Y-%m-%d")
locator = mdates.MonthLocator()
dlocator = mdates.DayLocator(interval=7)

audio = np.array(list(map(div, audio, members_discord)))
artists = np.array(list(map(div, artists, members_discord)))
programmers = np.array(list(map(div, programmers, members_discord)))
writers = np.array(list(map(div, writers, members_discord)))
moderators = np.array(list(map(div, moderators, members_discord)))
english = np.array(list(map(div, english, members_discord)))
french = np.array(list(map(div, french, members_discord)))
spanish = np.array(list(map(div, spanish, members_discord)))
german = np.array(list(map(div, german, members_discord)))
roleplayers = np.array(list(map(div, roleplayers, members_discord)))
community_project = np.array(list(map(div, community_project, members_discord)))
without_role = np.array(list(map(div, without_role, members_discord)))

plt.figure(1)

plt.plot_date(x_dates, members_jam, label='Members (Jam)', ls='solid')
plt.plot_date(x_dates, members_discord, label='Members (Discord)', ls='solid')
plt.xlabel('Date')
plt.ylabel('Members')
plt.title('Members (Jam & Discord)')

setup_xaxis()
setup_yaxis()

plt.legend()

plt.savefig('out/members.png')

plt.figure(2)

plt.plot_date(x_dates, submissions, label='Submissions', ls='solid')
plt.xlabel('Date')
plt.ylabel('Submissions')
plt.title('Submissions')

setup_xaxis()
setup_yaxis()

plt.legend()

plt.savefig('out/submissions.png')

plt.figure(3)

plt.plot_date(x_dates, audio, label='Audio Folks', ls='solid', c='b')
plt.plot_date(x_dates, artists, label='Artists', ls='solid', c='m')
plt.plot_date(x_dates, programmers, label='Programmers', ls='solid', c='c')
plt.plot_date(x_dates, writers, label='Writers', ls='solid', c='g')
plt.plot_date(x_dates, moderators, label='Moderators', ls='solid', c='y')
plt.plot_date(x_dates, roleplayers, label='Roleplayers', ls='solid', c='r')
plt.plot_date(x_dates, community_project, label='Community Project', ls='solid', c='fuchsia')
plt.plot_date(x_dates, without_role, label='Without A Role', ls='solid', c='grey')

plt.xlabel('Date')
plt.ylabel('Members with role (rel. to total member count)')
plt.title('Role Distribution')

setup_xaxis()
plt.gca().set_ylim([0,1])

plt.legend(bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0)

plt.savefig('out/roles.png', bbox_inches="tight")

plt.figure(4)

plt.plot_date(x_dates, english, label='English', ls='solid')
plt.plot_date(x_dates, french, label='French', ls='solid')
plt.plot_date(x_dates, spanish, label='Spanish', ls='solid')
plt.plot_date(x_dates, german, label='German', ls='solid')

plt.xlabel('Date')
plt.ylabel('Speakers (rle. to total member count)')
plt.title('Most Common Languages')

setup_xaxis()
setup_yaxis()

plt.legend()

if '--show' in sys.argv:
    plt.show()

plt.savefig('out/languages.png')